from django.shortcuts import render, redirect
from .forms import UserForm
from .models import User_pass


# Create your views here.

def index(request):
    print(request.method)
    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
        
    else:
        form = UserForm()
        data = {"form": form}

    return render(request, "index.html", context=data)
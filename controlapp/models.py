from django.db import models

# Create your models here.

class User_pass(models.Model):
    first_name = models.CharField(max_length=100)
    user_name = models.CharField(max_length=100)
    password = models.CharField(max_length=50)
    urls_1 = models.URLField(max_length=500)
    urls_2 = models.URLField(max_length=500)
    price = models.DecimalField(max_digits=5, decimal_places=2)
    description = models.TextField(max_length=1000)
    long_desc = models.TextField(max_length=1000)
    


    class Meta:
        db_table = "user_table"




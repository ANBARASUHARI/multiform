from django.forms import ModelForm
from .models import User_pass
from django import forms

class UserForm(forms.ModelForm):

    password = forms.CharField(widget=forms.PasswordInput)
    
    class Meta:
        model = User_pass
        fields = ["first_name", "user_name", "password", "urls_1", "urls_2", "price", "description", "long_desc"]
